package com.example.demo.rest;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthenticationRestControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private FilterChainProxy filterChainProxy;


    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(filterChainProxy)
                .build();
    }

    @Test
    public void create_authentication_token() throws Exception {
        String json = new JSONObject()
                .put("username", "test")
                .put("password", "test")
                .toString();

        this.mockMvc.perform(post("/app/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.token").isNotEmpty());
    }

    @Test
    public void try_login_if_user_does_not_exists() throws Exception {
        String json = new JSONObject()
                .put("username", "some_test")
                .put("password", "11081108")
                .toString();

        this.mockMvc.perform(post("/app/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void login_with_wrong_data() throws Exception {
        String json = new JSONObject()
                .put("username", "")
                .put("password", "11081108")
                .toString();

        this.mockMvc.perform(post("/app/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void login_with_wrong_password_data() throws Exception {
        String json = new JSONObject()
                .put("username", "test")
                .put("password", "")
                .toString();

        this.mockMvc.perform(post("/app/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

}