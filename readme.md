http://localhost:8087/



Database configuration

database Postgresql

username=postgres

password=postgres

url=jdbc:postgresql://localhost:5432/test



request in Postman

request type - POST

http://localhost:8087/app/auth/login

{
	"username":"test",
	"password":"test"
}

example response

{
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiY3JlYXRlZCI6MTUzMTQ2NTM5MjYxMSwiZXhwIjoxNTMyMDcwMTkyfQ.97Z9hEGvlGurOqEqt5M1AWqD86Mpkx86xK7GPqms3SU8jePj-tlmwvaYKk-0c8TsJKmxjvEsriGVDKQM5odSrA"
}